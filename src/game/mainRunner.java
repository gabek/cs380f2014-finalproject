package game;

import java.io.File;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Date;

import java.util.ArrayList;
import java.util.Scanner;

public class mainRunner
{
	public mainRunner()
	{
	}
	
	public void runner(String flag, int id)
	{
		//System.out.println("Please insert a player's id");
		
		//int idNum = scan.nextInt();

		DatabaseManager dm = new DatabaseManager();

		Scanner scan = new Scanner(System.in);

		PlayerData pd = new PlayerData(id, dm);


			//String flag = scan.nextLine();

			switch(flag)
			{
				case "-name": ArrayList<String> names = pd.avatarName();
							for(int i = 0; i < names.size(); i++)
							{
								System.out.print(names.get(i) +" ");
							}
							System.out.println("");
							break;

				case "-class": ArrayList<String> classes = pd.avatarClass();
							for(int i = 0; i < classes.size(); i++)
							{
								System.out.print(classes.get(i) +" ");
							}
							System.out.println("");
							break;

				case "-level": ArrayList<Integer> level = pd.avatarLevel();
							for(int i = 0; i < level.size(); i++)
							{
								System.out.print(level.get(i) +" ");
							}
							System.out.println("");
							break;

				case "-date": ArrayList<Date> date = pd.avatarDate();
							for(int i = 0; i < date.size(); i++)
							{
								System.out.print(date.get(i).toString() +" ");
							}
							System.out.println("");
							break;

				case "-avatar": System.out.println();
							  System.out.println("Insert a name");
							  String name = scan.nextLine();
							  System.out.println();
							ArrayList<String> avatar = pd.getAvatarData(name);
							for(int i = 0; i < avatar.size(); i++)
							{
								System.out.print(avatar.get(i) +" ");
							}
							System.out.println("");
							break;
				
				case "-player": ArrayList<String> player = pd.getPlayer();
							for(int i = 0; i < player.size(); i++)
							{
								System.out.print(player.get(i) +" ");
							}
							System.out.println("");
							break;

				case "-c": break;

				case "-e": break;

				
				default: System.out.println("Type -name to get all characters name by a player, " +
					"-class to get all classes played by the player, -level to get the levels the characters are at, " +
					"-date to get their creation dates, -avatar followed by name to get info about a specific avatar, " +
					"-player to get info about a single player, -c to change ids, and -e to exit.");
			}

		//return exit;
	}

	public static void main(String[] args)
	{
		mainRunner mr = new mainRunner();
		
		System.out.println("Player id: ");

		Scanner scan = new Scanner(System.in);

		int id = scan.nextInt();

		scan.nextLine();

		System.out.println("Type -name to get all characters name by a player, " +
					"-class to get all classes played by the player, -level to get the levels the characters are at, " +
					"-date to get their creation dates, -avatar followed by name to get info about a specific avatar, " +
					"-player to get info about a single player, -c to change ids, and -e to exit.");
		
		boolean exit = false;

		while(exit == false)
		{
			String flag = scan.nextLine();

			mr.runner(flag, id);

			if(flag.equals("-e"))
					{
						exit = true;
					}
			else if(flag.equals("-c"))
				{
					System.out.println("Player id: ");
					id = scan.nextInt();
					scan.nextLine();
				}
					
		}
			
	}
}

