package game;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({

    // DataGeneratorTest 
	TestMockito.class,
	TestDbunit.class,
	TestMain.class,

})

public class TestMaster {
}
