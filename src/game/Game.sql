DROP TABLE IF EXISTS Player CASCADE;

DROP TABLE IF EXISTS Avatar CASCADE;

CREATE TABLE Player
(
	Id INTEGER NOT NULL,

	First_Name VARCHAR(50) NOT NULL,

	Middle_Initial CHAR(1),

	Last_Name VARCHAR(50) NOT NULL,

	Gender CHAR(1) NOT NULL,

	Age INTEGER NOT NULL,

	PRIMARY KEY (Id),

	CHECK (Gender = 'M' OR Gender = 'F')
	);

	CREATE TABLE Avatar
(
	Avatar_Name VARCHAR(20) NOT NULL,

	Player_Id Integer NOT NULL,

	Class VARCHAR(20) NOT NULL,

	Avatar_Level INTEGER NOT NULL,

	Creation_Date Date NOT NULL,

	PRIMARY KEY (Avatar_Name),

	FOREIGN KEY (Player_Id) REFERENCES Player (Id),

	CHECK (Avatar_Level < 81 OR Avatar_Level > 0)

	);
