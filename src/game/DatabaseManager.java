package game;
/*
 * @Author Gabe Kelly
 * This is a middle manager
 * for interactions with DB
 * to make testing a lot easier
 */
import java.sql.*;


public class DatabaseManager
{
	public DatabaseManager()
	{
	}

	public ResultSet execute(String query) throws ClassNotFoundException, SQLException
	{
		Class.forName("org.hsqldb.jdbc.JDBCDriver");

            	// Connect to the database
            	// It will be create automatically if it does not yet exist
            	// 'testfiles' in the URL is the name of the database
            	// "SA" is the user name and "" is the (empty) password
            	Connection conn =
                	DriverManager.getConnection("jdbc:hsqldb:testfiles", "SA", "");

				

        // Create a statement object
        Statement stat = conn.createStatement();

        // Now execute the search query
        // UCASE: This is a case insensitive search
        // ESCAPE ':' is used so it can be easily searched for '\'
        ResultSet result = stat.executeQuery(query);

        conn.close();

        stat.close();

        return result;
	}

}
