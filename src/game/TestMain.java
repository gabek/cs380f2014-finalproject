package game;
import java.io.FileOutputStream;
import java.io.File;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;

import java.util.ArrayList;
//import java.util.Date;

import java.text.SimpleDateFormat;
import java.text.ParseException;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.*;
import org.dbunit.operation.DatabaseOperation;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.*;

import org.hsqldb.cmdline.SqlFile;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import org.mockito.ArgumentCaptor;
import org.mockito.stubbing.Answer;
import org.mockito.invocation.InvocationOnMock;
import static org.mockito.Mockito.*;

public class TestMain
{
	private void cleanlyInsertDataset(IDataSet dataSet) throws Exception {
  		IDatabaseTester databaseTester = new JdbcDatabaseTester(
      	"org.hsqldb.jdbc.JDBCDriver", "jdbc:hsqldb:maintest", "sa", "");
  		
  		databaseTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
  		
  		databaseTester.setDataSet(dataSet);
  		
  		databaseTester.onSetup();
		}
		
	private IDataSet readDataSet() throws Exception {
  		return new FlatXmlDataSetBuilder().build(new File("/home/k/kellyg/cs380labs/FINALPROJECT/src/game/test-seed.xml"));
	}

  	/*Iprotected DatabaseOperation getSetUpOperation() throws Exception {
   			return DatabaseOperation.REFRESH;
 			}*/
		
		@Before
		public void importDataSet() throws Exception {
  			IDataSet dataSet = readDataSet();
  			cleanlyInsertDataset(dataSet);
		}
		
		@BeforeClass
		public static void createSchema() throws Exception {
  		File file = new File("/home/k/kellyg/cs380labs/FINALPROJECT/src/game/Game.sql");

  		SqlFile sql = new SqlFile(file);
  		  		
  		sql.setConnection(DriverManager.getConnection("jdbc:hsqldb:maintest", "SA", "")); 
  		sql.execute();
  		}

  		/**
  		 * Test: Main
  		 * this tests the main
  		 * runner method and most of its paths.
  		 */
  		@Test
  		public void testMain() throws ClassNotFoundException, SQLException
		{
			mainRunner mr = new mainRunner();
			
			String flag = "-name";

			int id = 82712;

			mr.runner(flag, id);

			flag = "-class";

			mr.runner(flag, id);

			flag = "-level";

			mr.runner(flag, id);

			flag = "-date";

			mr.runner(flag, id);

			flag = "-player";

			mr.runner(flag, id);

			flag = "-c";
			
			mr.runner(flag, id);

			flag = "-e";

			mr.runner(flag, id);

			flag = "-";

			mr.runner(flag, id);

		}



  		@After
		public void getTearDownOperation() throws Exception {
  			 //return DatabaseOperation.NONE;
		}


}

