package game;
/**
 * @Author Gabe Kelly
 * This is a test suite that uses Mockito
 * to test my database application
 * This uses mockito
 */
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.junit.rules.ExpectedException;

import java.io.PrintStream;
//import java.io.ByteArrayOutputStream;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Arrays;
//import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.List;

import  org.mockito.ArgumentCaptor;
import  org.mockito.stubbing.Answer;
import  org.mockito.invocation.InvocationOnMock;
import  static org.mockito.Mockito.*;

import  java.text.SimpleDateFormat;
import  java.text.ParseException;

import  java.sql.*;

import 	game.PlayerData;
import 	game.DatabaseManager;

public class TestMockito
{
	private SimpleDateFormat timestampFormat;

    private DatabaseManager mockDM;
    
    @Rule
  	public ExpectedException exception = ExpectedException.none();
    

    @Before
    public void setUp() 
    	{
        timestampFormat = new SimpleDateFormat("yyyy-MM-dd");    
        mockDM = mock(DatabaseManager.class);
        }
	
	/**
	 * This method is not my own and is based off of
	 * a blog post on http://www.canoo.com/blog/2010/11/05/advanced-mocking-capturing-state-with-answer-and-captors/
	 */
    private static ResultSet makeResultSet(final List<String> Collumns, final List... rows) throws Exception 
    {
    ResultSet result = mock(ResultSet.class);
    final AtomicInteger currentIndex = new AtomicInteger(-1);
 
    when(result.next()).thenAnswer(new Answer() {
        public Object answer(InvocationOnMock aInvocation) throws Throwable {
            return currentIndex.incrementAndGet() < rows.length;
        }
    });
 
    final ArgumentCaptor<String> argument = ArgumentCaptor.forClass(String.class);
    Answer rowLookupAnswer = new Answer() {
        public Object answer(InvocationOnMock aInvocation) throws Throwable {
            int rowIndex = currentIndex.get();
            int columnIndex = Collumns.indexOf(argument.getValue());
            return rows[currentIndex.get()].get(columnIndex);
        }
    };
    when(result.getString(argument.capture())).thenAnswer(rowLookupAnswer);
    when(result.getLong(argument.capture())).thenAnswer(rowLookupAnswer);
    when(result.getDate(argument.capture())).thenAnswer(rowLookupAnswer);
    
    return result;
}

	
/**
 * Test PlayersAvatarTest
 * This tests the method
 * PlayersAvatar. Which lists
 * the all of the player current
 * avatars.
 * @Parameters none
 * @Return nothing
 **/

	@Test
	public void TestAvatarName() throws SQLException, ParseException, ClassNotFoundException, Exception
	{
		int id = 82712;

		PlayerData pd = new PlayerData(id, mockDM);

		//Date time1 = timestampFormat.parse("2012-10-11");

		//Date time2 = timestampFormat.parse("2011-11-10");

		ArrayList<String> list = new ArrayList<String>();

		list.add("Wotan Vali");

		list.add("Hi There");
		
		ResultSet rsmock = makeResultSet(
				Arrays.asList("Avatar_Name", "Player_Id", "Class", "Avatar_Level", "Creation_Date"),
				Arrays.asList("Wotan Vali", 82712, "Thief", 80, 1326258600000l),
				Arrays.asList("Hi There", 82712, "Barbarian", 43, 1294636260000l));
		 
		when(mockDM.execute("SELECT * FROM Avatar WHERE Player_Id =" + id + ";")).thenReturn(rsmock); 

		when(rsmock.next()).thenReturn(true).thenReturn(true).thenReturn(false);

		when(rsmock.getString("Avatar_Name")).thenReturn("Wotan Vali").thenReturn("Hi There");

		//when(pd.AvatarName()).thenReturn(list);

		//when(rsmock.getInt("Player_Id")).thenReturn(082712).thenReturn(082712);

		//when(rsmock.getString("Class")).thenReturn("Thief").thenReturn("Barbarian");

		//when(rsmock.getId("Avatar_Level")).thenReturn(80).thenReturn(43);

		//when(rsmock.getDate("Creation_Date")).thenReturn("2012-10-11").thenReturn("2011-11-10");
		
		ArrayList<String> actual = pd.avatarName();

		String expectedOne = "Wotan Vali";

		String expectedTwo = "Hi There";
		

		assertEquals(expectedOne, actual.get(0));

		assertEquals(expectedTwo, actual.get(1));
		
		/*catch(Exception e)
		{
		}*/

	}

/**
 * Test AvatarClass()
 * This tests the method
 * avatarClass which lists
 * the all of the classes played by
 * the player.
 * @Parameters none
 * @Return nothing
 **/

	@Test
	public void testAvatarClass() throws SQLException, ParseException, ClassNotFoundException, Exception
	{
		int id = 82712;

		PlayerData pd = new PlayerData(id, mockDM);

		//Date time1 = timestampFormat.parse("2012-10-11");

		//Date time2 = timestampFormat.parse("2011-11-10");

		ArrayList<String> list = new ArrayList<String>();

		list.add("Wotan Vali");

		list.add("Hi There");
		
		ResultSet rsmock = makeResultSet(
				Arrays.asList("Avatar_Name", "Player_Id", "Class", "Avatar_Level", "Creation_Date"),
				Arrays.asList("Wotan Vali", 82712, "Thief", 80, 1326258600000l),
				Arrays.asList("Hi There", 82712, "Barbarian", 43, 1294636260000l));
		 
		when(mockDM.execute("SELECT * FROM Avatar WHERE Player_Id =" + id + ";")).thenReturn(rsmock); 

		when(rsmock.next()).thenReturn(true).thenReturn(true).thenReturn(false);

		when(rsmock.getString("Class")).thenReturn("Thief").thenReturn("Barbarian");

		ArrayList<String> actual = pd.avatarClass();

		String expectedOne = "Thief";

		String expectedTwo = "Barbarian";

		assertEquals(expectedOne, actual.get(0));

		assertEquals(expectedTwo, actual.get(1));
		
		/*catch(Exception e)
		{
		}*/
	}

	/**
 	* Test Avatar()
	* This tests the method
	* avatarLevels which lists
 	* the all of the levels of the avatars played
 	* the player.
 	* @Parameters none
 	* @Return nothing
 	**/
	@Test
	public void testAvatarLevel() throws SQLException, ParseException, ClassNotFoundException, Exception
	{
		int id = 82712;

		PlayerData pd = new PlayerData(id, mockDM);

		//Date time1 = timestampFormat.parse("2012-10-11");

		//Date time2 = timestampFormat.parse("2011-11-10");

		ArrayList<Integer> actual = new ArrayList<Integer>();

		//list.add("Wotan Vali");

		//list.add("Hi There");
		
		ResultSet rsmock = makeResultSet(
				Arrays.asList("Avatar_Name", "Player_Id", "Class", "Avatar_Level", "Creation_Date"),
				Arrays.asList("Wotan Vali", 82712, "Thief", 80, 1326258600000l),
				Arrays.asList("Hi There", 82712, "Barbarian", 43, 1294636260000l));
		 
		when(mockDM.execute("SELECT * FROM Avatar WHERE Player_Id =" + id + ";")).thenReturn(rsmock); 

		when(rsmock.next()).thenReturn(true).thenReturn(true).thenReturn(false);

		when(rsmock.getInt("Avatar_Level")).thenReturn(80).thenReturn(43);

		Integer expected1 = 80;

		Integer expected2 = 43;

		actual = pd.avatarLevel();

		assertEquals(expected1, actual.get(0));

		assertEquals(expected2, actual.get(1));

	}

	/**
	 * Test: testAvatarDate()
	 * This tests the method
	 * avatar date which
	 * gets all of the player
	 * character's creation dates.
	 * @Parameters none
	 * @Return nothing
	 */
	@Test
	public void testAvatarDate() throws SQLException, ParseException, ClassNotFoundException, Exception
	{
		int id = 82712;

		PlayerData pd = new PlayerData(id, mockDM);

		//Date expected1 = timestampFormat.parse("2012-10-11");

		//Date expected2 = timestampFormat.parse("2011-11-10");

		ArrayList<Date> actual = new ArrayList<Date>();

		Date expected1 = new Date(1349928000000l);

		Date expected2 = new Date(1294636260000l);

		ResultSet rsmock = makeResultSet(
				Arrays.asList("Avatar_Name", "Player_Id", "Class", "Avatar_Level", "Creation_Date"),
				Arrays.asList("Wotan Vali", 82712, "Thief", 80, expected1),
				Arrays.asList("Hi There", 82712, "Barbarian", 43, expected2));

		when(mockDM.execute("SELECT * FROM Avatar WHERE Player_Id =" + id + ";")).thenReturn(rsmock); 

		when(rsmock.next()).thenReturn(true).thenReturn(true).thenReturn(false);

		when(rsmock.getDate("Creation_Date")).thenReturn(expected1).thenReturn(expected2);

		actual = pd.avatarDate();

		assertEquals(expected1, actual.get(0));

		assertEquals(expected2, actual.get(1));
	}

	/**
	 * Test: testGetAvatarData()
	 * @Params: none
	 * @Return: nothing
	 * This tests the method
	 * getAvatarData() which
	 * returns all avaible info
	 * about a players avatar
	 */
	@Test
	public void testGetAvatarData() throws SQLException, ParseException, ClassNotFoundException, Exception
	{
		int id = 82712;

		PlayerData pd = new PlayerData(id, mockDM);

		ArrayList<String> actual = new ArrayList<String>();
		
		String expectedName = "Wotan Vali";

		String expectedClass = "Thief";

		String expectedLevel = "80";

		Date time = new Date(1349928000000l);

		String expectedDate = time.toString();

		ResultSet rsmock = makeResultSet(
				Arrays.asList("Avatar_Name", "Player_Id", "Class", "Avatar_Level", "Creation_Date"),
				Arrays.asList("Wotan Vali", 82712, "Thief", 80, time));


		when(mockDM.execute("SELECT * FROM Avatar WHERE Player_Id =" + id + " AND Avatar_Name ='" + expectedName + "';")).thenReturn(rsmock);

		when(rsmock.next()).thenReturn(true);

		when(rsmock.getDate("Creation_Date")).thenReturn(time);

		when(rsmock.getString("Avatar_Name")).thenReturn(expectedName);

		when(rsmock.getString("Class")).thenReturn(expectedClass);

		when(rsmock.getInt("Avatar_Level")).thenReturn(80);

		actual = pd.getAvatarData(expectedName);

		assertEquals(expectedName, actual.get(0));

		assertEquals(expectedClass, actual.get(1));

		assertEquals(expectedLevel, actual.get(2));

		assertEquals(expectedDate, actual.get(3));
	}

	/**
	 * Test: testGetPlayer()
	 * @Params: none
	 * @Return: nothing
	 * This tests the method
	 * getPlayerData() which
	 * returns all avaible info
		 * about a player.
	 */

	@Test
	public void testGetPlayer() throws SQLException, ParseException, ClassNotFoundException, Exception
	{
		int id = 82712;

		PlayerData pd = new PlayerData(id, mockDM);

		ArrayList<String> actual = new ArrayList<String>();

		String expectedID = "82712";

		String expectedFirst = "Gabriel";

		String expectedM = "B";

		String expectedLast = "Kelly";

		String expectedGender = "M";

		String expectedAge = "20";

		ResultSet rsmock = makeResultSet(
				Arrays.asList("Id", "First_Name", "Middle_Initial", "Last_Name", "Gender", "Age"),
				Arrays.asList(82712, "Gabriel", "B", "Kelly", "M", 20));
		
		when(mockDM.execute("SELECT * FROM Player WHERE Id =" + id + ";")).thenReturn(rsmock);

		when(rsmock.next()).thenReturn(true);

		when(rsmock.getInt("Id")).thenReturn(82712);

		when(rsmock.getString("First_Name")).thenReturn("Gabriel");

		when(rsmock.getString("Middle_Initial")).thenReturn("B");

		when(rsmock.getString("Last_Name")).thenReturn("Kelly");

		when(rsmock.getString("Gender")).thenReturn("M");

		when(rsmock.getInt("Age")).thenReturn(20);

		actual = pd.getPlayer();

		assertEquals(expectedID, actual.get(0));

		assertEquals(expectedFirst, actual.get(1));

		assertEquals(expectedM, actual.get(2));

		assertEquals(expectedLast, actual.get(3));

		assertEquals(expectedGender, actual.get(4));

		assertEquals(expectedAge, actual.get(5));

	}
	





}
