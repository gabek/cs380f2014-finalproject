package game;
/*
 * An app that gets player data.
 */

import java.io.File;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Date;

import java.util.ArrayList;
import java.util.Scanner;
//import java.util.Date;

public class PlayerData
{
	private int id;

	protected DatabaseManager dm;

	public PlayerData(int id, DatabaseManager dm)
	{
		this.id = id;

		this.dm = dm;

	}

	/**
	 * Method: avatarName()
	 * @Parameters: None.
	 * @Return ArrayList<String>
	 * This is a method that gets the
	 * name of all of the player's characters.
	 */
	public ArrayList<String> avatarName()
	{
		ArrayList<String> list = new ArrayList<String>();
		String query = "SELECT * FROM Avatar WHERE Player_Id =" + id + ";";

		try {
				ResultSet result = dm.execute(query);

       while (result.next())
	   {
	   	   list.add(result.getString("Avatar_Name"));
	   }
		}
		catch(ClassNotFoundException e)
		{
		}
		catch(SQLException e)
		{
		}

	   return list;
	}

	/**
	 * Method: avatarClass()
	 * @Parameters: None.
	 * @Return ArrayList<String>
	 * This is a method that gets the
	 * classes of all of the player's characters.
	 */

	public ArrayList<String> avatarClass()
	{
		ArrayList<String> list = new ArrayList<String>();
		
		String query = "SELECT * FROM Avatar WHERE Player_Id =" + id + ";";

		try {
				ResultSet result = dm.execute(query);

      	 while (result.next())
	   		{
	   	   list.add(result.getString("Class"));
	   		}
		}
		catch(ClassNotFoundException e)
		{
		}
		catch(SQLException e)
		{
		}

	   return list;
	}
	
	/**
	 * Method: avatarLevel()
	 * @Parameters: None.
	 * @Return ArrayList<Integer>
	 * This is a method that gets the
	 * level of all of the player's characters.
	 */

	public ArrayList<Integer> avatarLevel()
	{
		ArrayList<Integer> list = new ArrayList<Integer>();
		
		String query = "SELECT * FROM Avatar WHERE Player_Id =" + id + ";";

		try {
				ResultSet result = dm.execute(query);

      	 while (result.next())
	   		{
	   	   list.add(result.getInt("Avatar_Level"));
	   		}
		}
		catch(ClassNotFoundException e)
		{
		}
		catch(SQLException e)
		{
		}

	   return list;
	}

	/**
	 * Method: avatarDate()
	 * @Parameters: none.
	 * @Return ArrayList<Date>
	 * This is a method that
	 * gets the creation dates of all
	 * of the player's characters.
	 */

	public ArrayList<Date> avatarDate()
	{
		ArrayList<Date> list = new ArrayList<Date>();
		
		String query = "SELECT * FROM Avatar WHERE Player_Id =" + id + ";";

		try {
				ResultSet result = dm.execute(query);

      	 while (result.next())
	   		{
	   	   		list.add(result.getDate("Creation_Date"));
	   		}
		}
		catch(ClassNotFoundException e)
		{
		}
		catch(SQLException e)
		{
		}

	   return list;
	}

	/**
	 * Method: getAvatarData()
	 * @Parameters: String name.
	 * @Return ArrayList<String>
	 * This is a method that gets
	 * all information about an avatar
	 * should it exist and is used by the player.
	 */
	public ArrayList<String> getAvatarData(String name)
	{
		ArrayList<String> list = new ArrayList<String>();

		String query = "SELECT * FROM Avatar WHERE Player_Id =" + id + " AND Avatar_Name ='" + name + "';";

		try{
				ResultSet result = dm.execute(query);

				result.next();

				list.add(result.getString("Avatar_Name"));

				list.add(result.getString("Class"));

				String level = result.getInt("Avatar_Level") + "";//seems easier to convert out here and add later with no method

				list.add(level);

				list.add(result.getDate("Creation_Date").toString());
		}
		catch(ClassNotFoundException e)
		{
		}
		catch(SQLException e)
		{
		}

		return list;
	}
	
	/**
	 * Method: getPlayerData()
	 * @Parameters: None
	 * @Return ArrayList<String>
	 * This is a method that
	 * gets the info on the player 
	 * as specified by PlayerData
	 */
	public ArrayList<String> getPlayer()
	{
		ArrayList<String> list = new ArrayList<String>();

		String query = "SELECT * FROM Player WHERE Id =" + id + ";";

		try{
				ResultSet result = dm.execute(query);

				result.next();

				String Id = result.getInt("Id") + "";

				list.add(Id);

				list.add(result.getString("First_Name"));
				
				list.add(result.getString("Middle_Initial"));

				list.add(result.getString("Last_Name"));

				list.add(result.getString("Gender"));

				String age = result.getInt("Age") + "";//again initialize out here

				list.add(age);
		}
		catch(ClassNotFoundException e)
		{
		}
		catch(SQLException e)
		{
		}

		return list;
	}

	/*public static void main(String[] args) 
	{
		System.out.println("Please insert a player's id");

		Scanner scan = new Scanner(System.in);
		
		int idNum = scan.nextInt();

		DatabaseManager dm = new DatabaseManager();

		PlayerData pd = new PlayerData(idNum, dm);

		boolean exit = false;

		while(exit == false)
		{
			System.out.println("Type -name to get all characters name by a player, " +
					"-class to get all classes played by the player, -level to get the levels the characters are at, " +
					"-date to get their creation dates, -avatar followed by name to get info about a specific avatar, " +
					"-player to get info about a single player, -c to change ids, and -e to exit.");

			String flag = scan.nextLine();

			switch(flag)
			{
				case "-name": ArrayList<String> names = pd.avatarName();
							for(int i = 0; i < names.size(); i++)
							{
								System.out.print(names.get(i) +" ");
							}
							System.out.println("");
							break;

				case "-class": ArrayList<String> classes = pd.avatarClass();
							for(int i = 0; i < classes.size(); i++)
							{
								System.out.print(classes.get(i) +" ");
							}
							System.out.println("");
							break;

				case "-level": ArrayList<Integer> level = pd.avatarLevel();
							for(int i = 0; i < level.size(); i++)
							{
								System.out.print(level.get(i) +" ");
							}
							System.out.println("");
							break;

				case "-date": ArrayList<Date> date = pd.avatarDate();
							for(int i = 0; i < date.size(); i++)
							{
								System.out.print(date.get(i).toString() +" ");
							}
							System.out.println("");
							break;

				case "-avatar": System.out.println();
							  System.out.println("Insert a name");
							  String name = scan.nextLine();
							  System.out.println();
							ArrayList<String> avatar = pd.getAvatarData(name);
							for(int i = 0; i < avatar.size(); i++)
							{
								System.out.print(avatar.get(i) +" ");
							}
							System.out.println("");
							break;
				
				case "-player": ArrayList<String> player = pd.getPlayer();
							for(int i = 0; i < player.size(); i++)
							{
								System.out.print(player.get(i) +" ");
							}
							System.out.println("");
							break;

				case "-e": exit = true;
						 break;

				case "-c": int id = scan.nextInt();
						 pd = new PlayerData(id, dm);
						 break;

				default: System.out.println("Type -name to get all characters name by a player, " +
					"-class to get all classes played by the player, -level to get the levels the characters are at, " +
					"-date to get their creation dates, -avatar followed by name to get info about a specific avatar, " +
					"-player to get info about a single player, -c to change ids, and -e to exit.");
			}
		}		

	}*/
}



                


