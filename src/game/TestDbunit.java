package game;
/*
 * @Author Gabe Kelly
 * This is a testing framework
 * for DBUnit.
 * It along with mockito serve as my
 * test framework.
 * Not finding db unit stuff.
 */

import java.io.FileOutputStream;
import java.io.File;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;

import java.util.ArrayList;
//import java.util.Date;

import java.text.SimpleDateFormat;
import java.text.ParseException;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.*;
import org.dbunit.operation.DatabaseOperation;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.*;

import org.hsqldb.cmdline.SqlFile;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

public class TestDbunit
{
	private SimpleDateFormat timestampFormat;

	private void cleanlyInsertDataset(IDataSet dataSet) throws Exception {
  		IDatabaseTester databaseTester = new JdbcDatabaseTester(
      	"org.hsqldb.jdbc.JDBCDriver", "jdbc:hsqldb:testfiles", "sa", "");
  		
  		databaseTester.setSetUpOperation(DatabaseOperation.CLEAN_INSERT);
  		
  		databaseTester.setDataSet(dataSet);
  		
  		databaseTester.onSetup();
		}
		
	private IDataSet readDataSet() throws Exception {
  		return new FlatXmlDataSetBuilder().build(new File("/home/k/kellyg/cs380labs/FINALPROJECT/src/game/test-seed.xml"));
	}

  	/*Iprotected DatabaseOperation getSetUpOperation() throws Exception {
   			return DatabaseOperation.REFRESH;
 			}*/
		
		@Before
		public void importDataSet() throws Exception {
  			IDataSet dataSet = readDataSet();
  			cleanlyInsertDataset(dataSet);
		}
		@Before
		public void setUp()
		{
			timestampFormat = new SimpleDateFormat("yyyy-MM-dd");
		}

		@BeforeClass
		public static void createSchema() throws Exception {
  		File file = new File("/home/k/kellyg/cs380labs/FINALPROJECT/src/game/Game.sql");

  		SqlFile sql = new SqlFile(file);
  		  		
  		sql.setConnection(DriverManager.getConnection("jdbc:hsqldb:testfiles", "SA", "")); 
  		sql.execute();
  		}

		/**
		 * Test This a test of the execute method of
		 * the database manager.
		 * @Return nothing.
		 */
  		@Test
  		public void testExecute() throws ClassNotFoundException, SQLException
		{
			DatabaseManager db = new DatabaseManager();
			
			String query = "SELECT * from Avatar WHERE Player_Id = 82712;";

			ResultSet rs = db.execute(query);

			String expected1 = "Wotan Vali";

			String expected2 = "Thief";

			String expected3 = "2012-10-11";

			rs.next();

			String actual1 = rs.getString("Avatar_Name");

			String actual2 = rs.getString("Class");

			String actual3 = rs.getString("Creation_Date");

			assertEquals(expected1, actual1);

			assertEquals(expected2, actual2);

			assertEquals(expected3, actual3);
		}


		/*
		 * The following tests are intergration tests.
		 * They test both db execute,
		 * and the pd methods. So more of close to system tests
		 * considering how small the app is.
		 */


		/**
		 *Test: this is a test of the AvatarName
		 * method of PlayerData.
		 * @Return nothing.
		 **/
		@Test
		public void testAvatarName() throws ClassNotFoundException, SQLException
		{
			DatabaseManager db = new DatabaseManager();

			int id = 82712;

			PlayerData pd = new PlayerData(id, db);

			ArrayList<String> actual = pd.avatarName();

			String expected1 = "Wotan Vali";

			String expected2 = "Clovus";

			Integer expectedSize = 2;

			Integer actualSize = actual.size();

			assertEquals(expected1, actual.get(0));

			assertEquals(expected2, actual.get(1));

			assertEquals(expectedSize, actualSize);
		}

		/**
		 *Test: this is a test of the avatarClass
		 * method of playerData.
		 * @Return nothing.
		 */
		@Test
		public void testAvatarClass() throws ClassNotFoundException, SQLException
		{
			DatabaseManager db = new DatabaseManager();

			int id = 82712;

			PlayerData pd = new PlayerData(id, db);

			ArrayList<String> actual = pd.avatarClass();

			String expected1 = "Thief";

			String expected2 = "Monk";

			Integer expectedSize = 2;

			Integer actualSize = actual.size();

			assertEquals(expected1, actual.get(0));

			assertEquals(expected2, actual.get(1));

			assertEquals(expectedSize, actualSize);
		}

		/**
		 * Test: This is a test of the avatarLevel
		 * method of PlayerData.
		 * @Return nothing.
		 */
		@Test
		public void testAvatarLevel() throws ClassNotFoundException, SQLException
		{
			DatabaseManager db = new DatabaseManager();

			int id = 82712;

			PlayerData pd = new PlayerData(id, db);

			ArrayList<Integer> actual = pd.avatarLevel();

			Integer expected1 = 80;

			Integer expected2 = 20;

			Integer expectedSize = 2;

			Integer actualSize = actual.size();

			assertEquals(expected1, actual.get(0));

			assertEquals(expected2, actual.get(1));

			assertEquals(expectedSize, actualSize);
		}

		/**
		 * Test: This is a test of the avatarDate
		 * method of PlayerData
		 * @Return nothing.
		 */
		@Test
		public void testAvatarDate() throws ClassNotFoundException, SQLException, ParseException
		{
			DatabaseManager db = new DatabaseManager();

			int id = 82712;

			PlayerData pd = new PlayerData(id, db);

			ArrayList<Date> actual = pd.avatarDate();

			Date expected1 = new Date(1349928000000l);

			Date expected2 = new Date(1379044800000l);

			Integer expectedSize = 2;

			Integer actualSize = actual.size();
			
			assertEquals(expected1, actual.get(0));

			assertEquals(expected2, actual.get(1));

			assertEquals(expectedSize, actualSize);
		}

		/**
		 * Test: this is atest of the getAvatarData
		 * method of PlayerData
		 * @Return nothing.
		 */
		@Test
		public void testGetAvatarData() throws ClassNotFoundException, SQLException
		{
			DatabaseManager db = new DatabaseManager();

			int id = 231;

			String expectedName = "RoaringThunder";

			String expectedClass = "Elementalist";

			String expectedLevel = 80 + "";

			String expectedDate = "2011-12-12";

			PlayerData pd = new PlayerData(id, db);

			ArrayList<String> actual = pd.getAvatarData(expectedName);

			Integer expectedSize = 4;

			Integer actualSize = actual.size();
			
			assertEquals(expectedName, actual.get(0));

			assertEquals(expectedClass, actual.get(1));

			assertEquals(expectedLevel, actual.get(2));

			assertEquals(expectedDate, actual.get(3));

			assertEquals(expectedSize, actualSize);
		}
		
		/**
		 * Test: this is a test of the getPlayer
		 * method of PLayerData
		 * @Return nothing
		 */
		@Test
		public void testGetPlayer() throws ClassNotFoundException, SQLException
		{

			DatabaseManager db = new DatabaseManager();

			int id1 = 82712;

			int id2 = 231;

			PlayerData pd1 = new PlayerData(id1, db);

			PlayerData pd2 = new PlayerData(id2, db);

			ArrayList<String> actual1 = pd1.getPlayer();

			String expectedID1 = "82712";

			String expectedFirst1 = "Gabriel";

			String expectedM1 = "B";

			String expectedLast1 = "Kelly";

			String expectedGender1 = "M";

			String expectedAge1 = "20";

			Integer expectedSize1 = 6;

			String expectedID2 = "231";

			String expectedFirst2 = "Jane";

			String expectedM2 = " ";

			String expectedLast2 = "Doe";

			String expectedGender2 = "F";

			String expectedAge2 = "30";

			Integer expectedSize2 = 6;

			ArrayList<String> actual2 = pd2.getPlayer();//i run this to check that the null check works

			Integer actualSize1 = actual1.size();

			Integer actualSize2 = actual2.size();

			assertEquals(expectedID1, actual1.get(0));//1

			assertEquals(expectedFirst1, actual1.get(1));//2

			assertEquals(expectedM1, actual1.get(2));//3

			assertEquals(expectedLast1, actual1.get(3));//4

			assertEquals(expectedGender1, actual1.get(4));//5

			assertEquals(expectedAge1, actual1.get(5));//6

			assertEquals(expectedID2, actual2.get(0));//7

			assertEquals(expectedFirst2, actual2.get(1));//8

			assertEquals(expectedM2, actual2.get(2));//14

			assertEquals(expectedLast2, actual2.get(3));//9

			assertEquals(expectedGender2, actual2.get(4));//10

			assertEquals(expectedAge2, actual2.get(5));//11

			assertEquals(expectedSize1, actualSize1);//12

			assertEquals(expectedSize2, actualSize2);//13. Let see if it can beat them all
		}

				
		@After
		public void getTearDownOperation() throws Exception {
  			 //return DatabaseOperation.NONE;
		}


}
