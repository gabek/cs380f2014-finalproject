# README #

This is my Final Project from Principles of Database Organization from Fall 2014. 

##Final Project##
This final project is about creating a database application and implementing a Test Suite to test the application.

##What is Used##
SQL is used as the database language. Java is the programming langauge. Tools for creating the test suite are Junit, Mockito, and DBUnit.

##Contact##
kellyg@allegheny.edu